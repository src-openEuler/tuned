Summary: A system tuning service for Linux
Name: tuned
Version: 2.24.1
Release: 1
License: GPLv2+
Source0: https://github.com/redhat-performance/%{name}/archive/refs/tags/v%{version}.tar.gz#/%{name}-%{version}.tar.gz
URL: http://www.tuned-project.org/

BuildArch: noarch

%bcond_without python3
%global _py python3
%global docdir %{_docdir}/%{name}

Requires(post): systemd, virt-what
Requires(preun): systemd
Requires(postun): systemd
BuildRequires: %{_py}, %{_py}-devel
BuildRequires:  systemd, desktop-file-utils

Requires: python3-dbus, util-linux, systemtap
Requires: virt-what, gawk, dbus, ethtool
Requires: polkit, powertop, hdparm, python3-gobject-base

Requires: %{_py}-configobj, %{_py}-pyudev, %{_py}-linux-procfs
Requires: %{_py}-schedutils, %{_py}-decorator, %{_py}-perf
Recommends:kernel-tools
Recommends:tuned-profiles-nfv-host-bin

Patch0: bugfix-tuned-2.8.0-restart-after-kill-dbus.patch
#the below two patches is from 
#http://ftp.iij.ad.jp/pub/linux/centos-vault/centos/8-stream/BaseOS/Source/SPackages/tuned-2.19.0-1.el8.src.rpm
#this is used to compatible with old tuned version like 2.10
Patch1: tuned-2.18.0-rhel-8-profiles.patch 
Patch2: tuned-2.18.0-sd-load-balance.patch
Patch3: change-the-default-percentage-when-dirty-data-starts.patch
Patch4: tuned-add-app-sensor-profile.patch
Patch5: profiles-drop-sched_-tuning-where-appropriate.patch
Patch6: bugfix-check_positive-has-some-contradictions.patch
Patch7: bugfix-expand-variables-in-Plugin.patch

Provides:     tuned-gtk = %{version}-%{release}
Provides:     tuned-utils = %{version}-%{release}
Provides:     tuned-profiles-sap = %{version}-%{release}
Provides:     tuned-profiles-mssql = %{version}-%{release}
Provides:     tuned-profiles-oracle = %{version}-%{release}
Provides:     tuned-profiles-sap-hana = %{version}-%{release}
Provides:     tuned-profiles-atomic = %{version}-%{release}
Provides:     tuned-profiles-nfv = %{version}-%{release}
Provides:     tuned-profiles-cpu-partitioning = %{version}-%{release}
Provides:     tuned-profiles-compat = %{version}-%{release}
Provides:     tuned-utils-systemtap = %{version}-%{release}

Obsoletes:    tuned-gtk < %{version}-%{release}
Obsoletes:    tuned-utils < %{version}-%{release}
Obsoletes:    tuned-profiles-sap < %{version}-%{release}
Obsoletes:    tuned-profiles-mssql < %{version}-%{release}
Obsoletes:    tuned-profiles-oracle < %{version}-%{release}
Obsoletes:    tuned-profiles-sap-hana < %{version}-%{release}
Obsoletes:    tuned-profiles-atomic < %{version}-%{release}
Obsoletes:    tuned-profiles-nfv < %{version}-%{release}
Obsoletes:    tuned-profiles-cpu-partitioning < %{version}-%{release}
Obsoletes:    tuned-profiles-compat < %{version}-%{release}
Obsoletes:    tuned-utils-systemtap < %{version}-%{release}

%description
Tuned is a daemon that uses udev to monitor connected devices and
statically and dynamically tunes system settings according to a
selected profile. It is distributed with a number of predefined
profiles for common use cases like high throughput, low latency,
or powersave, and allows you to further alter the rules defined
for each profile and customize how to tune a particular device.
To revert all changes made to the system settings by a certain
profile, you can either switch to another profile or deactivate
the tuned daemon.

gtk
gtk GTK GUI that can alter the rules defined for each profile and
customize how to tune a particular device.

profiles-mssql
Description of profile provided for the MS SQL Server. This
profileis provided for the MS SQL Server. It's based on the
throughput-performance profile.

utils-systemtap
The package is used to monitor system status by some systemtap scripts.

utils
The command line utilities for tuned

profiles-sap
Description of profiles provided for the SAP NetWeaver. These profiles
provides performance optimizations for the SAP NetWeaver applications.

profiles-oracle
Description of profile provided for Oracle.

profiles-realtime
Description of profiles provided for the realtime.

profiles-atomic
Description of profiles provided for the Project Atomic.These
profiles are provided for the Project Atomic. They provides
performance optimizations for the Atomic hosts (bare metal) and
virtual guests.

profiles-sap-hana
An in-memory, highly scalable database that is
particularly well suited to analytics.

profiles-nfv-guest
Additional tuned profile(s) targeted to Network Function Virtualization (NFV) guest.

profiles-nfv-host
Additional tuned profile(s) targeted to Network Function Virtualization (NFV) host.

profiles-nfv
Additional tuned profile(s) targeted to Network Function Virtualization (NFV).

profiles-cpu-partitioning
Additional tuned profile(s) optimized for CPU partitioning.

profiles-compat
Additional tuned profiles mainly for backward compatibility with tuned 1.0.
It can be also used to fine tune your system for specific scenarios.

%package    profiles-devel
Summary:    Additional tuned profile(s) to realtime, NFV guest and NFV host.
Requires: %{name} = %{version}
Requires: tuna
Recommends: tuned-profiles-nfv-host-bin


Provides:     tuned-profiles-realtime = %{version}-%{release}
Provides:     tuned-profiles-nfv-guest = %{version}-%{release}
Provides:     tuned-profiles-nfv-host = %{version}-%{release}

Obsoletes:    tuned-profiles-realtime < %{version}-%{release}
Obsoletes:    tuned-profiles-nfv-guest < %{version}-%{release}
Obsoletes:    tuned-profiles-nfv-host < %{version}-%{release}

%description    profiles-devel
Man pages and other related documents for %{name}

%package    help
Summary:    Documents for %{name}
Requires:   man, info

%description    help
Man pages and other related documents for %{name}


%prep
%autosetup -n %{name}-%{version} -p1

%build

%install
%make_install DOCDIR=%{docdir}
PYTHON=%{__python3}
sed -i 's/\(dynamic_tuning[ \t]*=[ \t]*\).*/\10/' %{buildroot}%{_sysconfdir}/%{name}/tuned-main.conf

mkdir -p %{buildroot}%{_datadir}/%{name}/grub2
mv %{buildroot}%{_sysconfdir}/grub.d/00_tuned %{buildroot}%{_datadir}/%{name}/grub2/00_tuned
rmdir %{buildroot}%{_sysconfdir}/grub.d

mkdir -p %{buildroot}%{_var}/lib/%{name}

mkdir -p %{buildroot}%{_sysconfdir}/modprobe.d
touch %{buildroot}%{_sysconfdir}/modprobe.d/kvm.rt.tuned.conf

desktop-file-validate %{buildroot}%{_datadir}/applications/tuned-gui.desktop

%post
if [ $1 -eq 1 ] ; then
        systemctl preset %{name}.service >/dev/null 2>&1 || :
fi

sed -i 's|.*/\([^/]\+\)/[^\.]\+\.conf|\1|' %{_sysconfdir}/%{name}/active_profile

if [ -r "%{_sysconfdir}/default/grub" ]; then
  sed -i 's/GRUB_CMDLINE_LINUX="$GRUB_CMDLINE_LINUX \\$tuned_params"/GRUB_CMDLINE_LINUX_DEFAULT="$GRUB_CMDLINE_LINUX_DEFAULT \\$tuned_params"/' \
    %{_sysconfdir}/default/grub
fi

%preun
if [ $1 -eq 0 ] ; then
        systemctl --no-reload disable %{name}.service > /dev/null 2>&1 || :
        systemctl stop %{name}.service > /dev/null 2>&1 || :
fi


if [ "$1" == 0 ]; then
  rm -f %{_var}/lib/%{name}/*
  rm -f /run/%{name}/*
fi

%postun
systemctl daemon-reload >/dev/null 2>&1 || :
if [ $1 -ge 1 ] ; then
        systemctl try-restart %{name}.service >/dev/null 2>&1 || :
fi

if [ "$1" == 0 ]; then
  rm -f %{_sysconfdir}/grub.d/00_tuned || :
  if [ -r "%{_sysconfdir}/default/grub" ]; then
    sed -i '/GRUB_CMDLINE_LINUX_DEFAULT="${GRUB_CMDLINE_LINUX_DEFAULT:+$GRUB_CMDLINE_LINUX_DEFAULT }\\$tuned_params"/d' %{_sysconfdir}/default/grub
  fi
fi

%triggerun -- %{name} < 2.0-0
/usr/sbin/service ktune stop &>/dev/null || :
/usr/sbin/chkconfig --del ktune &>/dev/null || :

%posttrans
if [ -d %{_sysconfdir}/grub.d ]; then
  cp -a %{_datadir}/%{name}/grub2/00_tuned %{_sysconfdir}/grub.d/00_tuned
  selinuxenabled &>/dev/null && \
    restorecon %{_sysconfdir}/grub.d/00_tuned &>/dev/null || :
fi

%files
%doc %{docdir}
%dir %{_datadir}/%{name}
%dir %{_sysconfdir}/%{name}
%dir %{_sysconfdir}/%{name}/recommend.d
%dir %{_libexecdir}/%{name}
%dir %{_localstatedir}/log/%{name}
%dir %{_var}/lib/%{name}
%dir /run/%{name}
%{_datadir}/bash-completion/completions/%{name}-adm
%{_datadir}/%{name}/grub2
%{_datadir}/polkit-1/actions/com.redhat.%{name}.policy
%{_datadir}/%{name}/ui
%{_datadir}/icons/hicolor/scalable/apps/%{name}.svg
%{_datadir}/applications/%{name}-gui.desktop
%{_sbindir}/%{name}
%{_sbindir}/%{name}-adm
%{_sbindir}/%{name}-gui
%{_sbindir}/varnetload
%{_sbindir}/netdevstat
%{_sbindir}/diskdevstat
%{_sbindir}/scomes

%exclude %{_prefix}/lib/%{name}/profiles/realtime
%exclude %{_prefix}/lib/%{name}/profiles/realtime-virtual-guest
%exclude %{_prefix}/lib/%{name}/profiles/realtime-virtual-host
%{_prefix}/lib/%{name}

%config(noreplace) %{_sysconfdir}/%{name}/cpu-partitioning-variables.conf
%config(noreplace) %{_sysconfdir}/%{name}/cpu-partitioning-powersave-variables.conf
%config(noreplace) %{_sysconfdir}/%{name}/%{name}-main.conf
%config(noreplace) %verify(not size mtime md5) %{_sysconfdir}/%{name}/active_profile
%config(noreplace) %verify(not size mtime md5) %{_sysconfdir}/%{name}/profile_mode
%config(noreplace) %verify(not size mtime md5) %{_sysconfdir}/%{name}/bootcmdline
%ghost %{_sysconfdir}/modprobe.d/kvm.rt.%{name}.conf
%{_bindir}/powertop2%{name}
%{_libexecdir}/%{name}/defirqaffinity*
%{_libexecdir}/%{name}/pmqos-static*
%{python3_sitelib}/%{name}
%{_datadir}/dbus-1/system.d/com.redhat.%{name}.conf
%verify(not size mtime md5) %{_sysconfdir}/modprobe.d/%{name}.conf
%{_tmpfilesdir}/%{name}.conf
%{_unitdir}/%{name}.service

%files help
%{_mandir}/man5/%{name}*
%{_mandir}/man7/%{name}-profiles-mssql.7*
%{_mandir}/man7/%{name}-profiles-sap-hana.7*
%{_mandir}/man7/%{name}-profiles-sap.7*
%{_mandir}/man7/%{name}-profiles-cpu-partitioning.7*
%{_mandir}/man7/%{name}-profiles-oracle.7*
%{_mandir}/man7/%{name}-profiles-atomic.7*
%{_mandir}/man7/%{name}-profiles-compat.7*
%{_mandir}/man7/%{name}-profiles.7*
%{_mandir}/man8/%{name}*
%{_mandir}/man8/varnetload.*
%{_mandir}/man8/netdevstat.*
%{_mandir}/man8/diskdevstat.*
%{_mandir}/man8/scomes.*

%files profiles-devel
%config(noreplace) %{_sysconfdir}/%{name}/realtime-variables.conf
%{_prefix}/lib/%{name}/profiles/realtime
%config(noreplace) %{_sysconfdir}/%{name}/realtime-virtual-guest-variables.conf
%{_prefix}/lib/%{name}/profiles/realtime-virtual-guest
%config(noreplace) %{_sysconfdir}/%{name}/realtime-virtual-host-variables.conf
%{_prefix}/lib/%{name}/profiles/realtime-virtual-host
%{_mandir}/man7/%{name}-profiles-realtime.7*
%{_mandir}/man7/%{name}-profiles-nfv-guest.7*
%{_mandir}/man7/%{name}-profiles-nfv-host.7*
%config(noreplace) %verify(not size mtime md5) %{_sysconfdir}/tuned/post_loaded_profile
%{_prefix}/lib/kernel/install.d/92-tuned.install
%{_mandir}/man7/tuned-profiles-openshift.7*
%{_mandir}/man7/tuned-profiles-postgresql.7*
%{_mandir}/man7/tuned-profiles-spectrumscale-ece.7*

%changelog
* Wed Nov 27 2024 ZhaoYu Jiang <jiangzhaoyu@kylinos.cn> - 2.24.1-1
- Upgrade to 2.24.1:
  - fixed privileged execution of arbitrary scripts by active local user. (CVE-2024-52336)
  - added sanity checks for API methods parameters. (CVE-2024-52337)
  - tuned-ppd: fixed controller init to correctly set_on_battery

* Fri Sep 20 2024 dufuhang <dufuhang@kylinos.cn> - 2.20.0-4
- bugfix: expand variables in Plugin._verify_all_device_commands

* Thu Sep 19 2024 dufuhang <dufuhang@kylinos.cn> - 2.20.0-3
- bugfix: check_positive has some contradictions

* Fri Apr 12 2024 liuchao <liuchao173@huawei.com> - 2.20.0-2
- profiles: drop sched_ tuning where appropriate

* Tue Feb 27 2024 liuchao <liuchao173@huawei.com> - 2.20.0-1
- downgrade to 2.20.0 to match the 24.03 LTS's minimum set of core packages

* Thu Jan 25 2024 liuchao <liuchao173@huawei.com> - 2.21.0-1
- upgrade to 2.21.0:
  - api: fixed stop method not to require any parameter
  - plugin_scheduler: fix perf fd leaks
  - allow skipping rollback when restarting TuneD or switching profile
  - function_calc_isolated_cores: no errors for offline CPUs
  - sap-hana: new profile sap-hana-kvm-guest
  - serialized SIGHUP handler to prevent possible bootcmdline corruption
  - fixed possible traceback on SIGHUP
  - updated manual pages to be consistent
  - tuned-adm: better error message for unauthorized switch_profile
  - plugin_sysctl: report reapplied sysctls only on different values
  - plugin_cpu: added support for pm_qos_resume_latency_us
  - plugin_cpu: added support for intel_pstate scaling driver
  - plugin_scheduler: do not leak FDs from the perf
  - profiles: new cpu-partitioning-powersave profile
  - profiles: new profile for AWS EC2
  - throughput-performance: set net.core.somaxconn to at least 2048
  - API: added support for moving devices between instances
  - added support for the API access through the Unix Domain Socket
  - D-Bus: send tracebacks through D-Bus only in debug mode
  - Makefile: added fix for python-3.12
  - do not exit on duplicate config lines

* Sat Jul 15 2023 hubin <hubin73@huawei.com> - 2.19.0-5
- tuned: add application plugin and process monitor for app-sensor profile

* Sat Jan 28 2023 Cao Jingbo <caojb@chinatelecom.cn> - 2.19.0-4
- Fix files list twice warning.
- Fix obsolete warning.

* Mon Dec 26 2022 hongrongxuan <hongrongxuan@huawei.com> - 2.19.0-3
- change the default percentage when dirty data starts writeback

* Tue Nov 8 2022 Qingqing Li <liqingqing3@huawei.com> - 2.19.0-2
- backport some patches to compatible with version 2.10

* Fri Sep 23 2022 langfei <langfei@huawei.com> - 2.19.0-1
- upgrade to 2.19.0

* Fri Jul 30 2021 chenyanpanHW <chenyanpan@huawei.com> - 2.10.0-10
- DESC: delete -Sgit from %autosetup, and delete BuildRequires git

* Fri Mar 05 2021 xinghe <xinghe1@huawei.com> - 2.10.0-9
- powertop2tuned work with PowerTOP 2.13 and newer

* Fri Sep 18 2020 xinghe <xinghe1@huawei.com> - 2.10.0-8
- cpu Update checking if EPB is supported

* Sun Sep 7 2019 hejingxian<hejingxian@huawei.com> - 2.10.0-7
- Type:other
- ID:NA
- SUG:NA
- DESC: new package tuned-profiles-devel

* Wed Aug 28 2019 caomeng<caomeng5@huawei.com> - 2.10.0-6
- Type:other
- ID:NA
- SUG:NA
- DESC:openeuler rewrite spec file

* Wed Aug 28 2019 caomeng<caomeng5@huawei.com> - 2.10.0-5
- Package init


